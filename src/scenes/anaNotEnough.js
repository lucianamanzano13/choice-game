module.exports = {
    name: 'anaNotEnough',
    type: 'choice',
    data: [
        {
            text: 'ANA dice:',
            next: 'anaNotEnough'
        },
        {
            text: '¡Muy Bien! Pero no es suficiente.',
            next: 'anaNotEnough'
        },
        {
            text: '¿Qué le decís?',
            next: 'anaNotEnough'
        },
        {
            text: ' - ¡Voy a seguir bajando de peso!',
            next: 'anaPressure'
        },
        {
            text: ' - ¡Dejame en paz!',
            next: 'anaPressure'
        }
    ]
};
