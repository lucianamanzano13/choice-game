const requireModule = require.context('./', false, /\.js$/);

const scenes = [];
let sceneIndex = 0;

requireModule.keys().forEach((file) => {
    console.log(file)
    if (file.includes('index.js')) {
        return;
    }

    const sceneFileData = require(`${file}`);

    scenes.push({
        key: sceneIndex++,
        name: sceneFileData.name ?? '',

        type: sceneFileData.type ?? 'empty',
        data: sceneFileData.data ?? {},

        Extras: sceneFileData.extras,

        dataStyle: sceneFileData.dataStyles ?? {},
    });
});

module.exports = scenes;
