module.exports = {
    name: 'anaHelp',
    type: 'choice',
    data: [
        {
            text: 'ANA te va a ayudar.',
            next: 'anaHelp'
        },
        {
            text: '¿Cómo vas a adelgazar?',
            next: 'anaHelp'
        },
        {
            text: ' - Contando calorías',
            next: 'anaNotEnough'
        },
        {
            text: ' - Dejando de comer',
            next: 'anaNotEnough'
        }
    ]
};
