module.exports = {
    name: 'anaOutfit',
    type: 'choice',
    data: [
        {
            title: '¿Cuál es tu outfit favorito?',
            next: 'anaOutfit'
        },
        {
            text: ' - Crop top y jean tiro bajo',
            next: 'anaCorrect'
        },
        {
            text: ' - Crop top y jean tiro alto',
            next: 'anaWrong'
        }
    ]
};
