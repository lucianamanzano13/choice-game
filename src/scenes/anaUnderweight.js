module.exports = {
    name: 'anaUnderweight',
    type: 'choice',
    data: [
        {
            text: 'Pasaste mucho tiempo con ANA y ahora estás por debajo de tu peso sano',
            next: 'anaUnderweight'
        },
        {
            text: '¿Qué hacés?',
            next: 'anaUnderweight'
        },
        {
            text: ' - Ir al médico',
            next: 'longTime'
        },
        {
            text: ' - Seguir hablando con ANA',
            next: 'longTime'
        }
    ]
};
