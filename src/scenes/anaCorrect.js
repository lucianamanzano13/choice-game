module.exports = {
    name: 'anaCorrect',
    type: 'choice',
    data: [
        {
            text: 'ANA dice:',
            next: 'anaCorrect'
        },
        {
            text: '¡Buena Elección!\nPero no te queda lindo...',
            next: 'anaCorrect'
        },
        {
            text: '¿Qué hacés?',
            next: 'anaCorrect'
        },
        {
            text: ' - Perder Peso',
            next: 'anaHelp'
        },
        {
            text: ' - Hablar con ANA al respecto',
            next: 'anaWeightLoss'
        }
    ]
};
