module.exports = {
    name: 'anaWrong',
    type: 'choice',
    data: [
        {
            text: 'ANA dice:',
            next: 'mainBranch'
        },
        {
            text: 'Opción INCORRECTA',
            next: 'mainBranch'
        },
        {
            title: 'INTENTA DE NUEVO',
            next: 'mainBranch'
        }
    ]
};
