module.exports = {
    name: 'anaWeightLoss',
    type: 'choice',
    data: [
        {
            text: 'ANA dice que TENÉS que perder peso porque todo te queda feo',
            next: 'anaWeightLoss'
        },
        {
            text: '¿Qué hacés?',
            next: 'anaWeightLoss'
        },
        {
            text: ' - Perder Peso',
            next: 'anaNotEnough'
        },
        {
            text: ' - No escuchar a ANA',
            next: 'anaPressure'
        }
    ]
};
