import Extras from '../../../sceneExtras';

import './styles.css';

function MenuItem (props) {
    const goNext = () => {
        if (props.onTransition) {
            props.onTransition(props.next ?? 'main');
        }
    };

    return (
        <p style={{
            fontSize: props.size ?? '24px',
            color: props.color ?? '#fff'
        }}
            onClick={goNext}
        >
            {props.text}
        </p>
    );
}

export default function MenuScene (props) {
    const goNext = (name) => {
        if (props.onTransition) {
            props.onTransition(name ?? 'main');
        }
    };

    return (
        <div className='MenuScene'
             style={props.dataStyle}
        >
            {
                props.data.map((e, i) => (
                    <MenuItem
                        key={i}
                        onTransition={goNext}
                        {...e}
                    />
                ))
            }

            <Extras name={props.Extras} />

        </div>
    );
}
